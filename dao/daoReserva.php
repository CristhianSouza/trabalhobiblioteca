<?php
/**
 * Created by PhpStorm.
 * User: tassio
 * Date: 2019-03-16
 * Time: 15:39
 */

require_once "iPage.php";

class daoReserva implements iPage
{

    public function remover($source)
    {
        try {
            $statement = Conexao::getInstance()->prepare("DELETE FROM tb_reserva WHERE id_reserva = :id");
            $statement->bindValue(":id", $source);
            if ($statement->execute()) {
                return "<script> alert('Reserva cancelada com êxito !'); </script>";
            } else {
                throw new PDOException("<script> alert('Não foi possível executar a declaração SQL !'); </script>");
            }
        } catch (PDOException $erro) {
            return "Erro: " . $erro->getMessage();
        }
    }

    public function obterReservadosPenultimoMes(){
        try {
            $statement = Conexao::getInstance()->prepare("SELECT COUNT(emp.id_reserva) total
                                                               , MONTHNAME(STR_TO_DATE(MONTH(now())-1, '%m')) mes
                                                            FROM tb_reserva emp
                                                           WHERE MONTH(emp.data_reserva) = MONTH(now())-1");
            if ($statement->execute()) {
                return $statement->fetch(PDO::FETCH_OBJ);
            } else {
                throw new PDOException("<script> alert('Não foi possível executar a declaração SQL !'); </script>");
            }
        } catch (PDOException $erro) {
            return "Erro: " . $erro->getMessage();
        }
    }

    public function obterReservadosAntePenultimoMes(){
        try {
            $statement = Conexao::getInstance()->prepare("SELECT COUNT(emp.id_reserva) total
                                                               , MONTHNAME(STR_TO_DATE(MONTH(now())-2, '%m')) mes
                                                            FROM tb_reserva emp
                                                           WHERE MONTH(emp.data_reserva) = MONTH(now())-2");
            if ($statement->execute()) {
                return $statement->fetch(PDO::FETCH_OBJ);;
            } else {
                throw new PDOException("<script> alert('Não foi possível executar a declaração SQL !'); </script>");
            }
        } catch (PDOException $erro) {
            return "Erro: " . $erro->getMessage();
        }
    }

    public function obterReservadosPorCategoria(){
        try {
            $statement = Conexao::getInstance()->prepare("SELECT COUNT(emp.id_reserva) total
                                                               , cat.nomeCategoria
                                                            FROM tb_categoria cat
                                                            LEFT JOIN tb_livro liv
                                                              ON liv.tb_categoria_idtb_categoria = cat.idtb_categoria
                                                            LEFT JOIN tb_exemplar exe
                                                              ON exe.tb_livro_idtb_livro = liv.idtb_livro
                                                            LEFT JOIN tb_reserva emp
                                                              ON emp.id_exemplar = exe.idtb_exemplar    
                                                           WHERE MONTH(emp.data_reserva) > MONTH(now())-3
                                                              OR emp.data_reserva IS NULL
                                                           GROUP BY cat.idtb_categoria");
            if ($statement->execute()) {
                return $statement->fetchAll(PDO::FETCH_OBJ);
            } else {
                throw new PDOException("<script> alert('Não foi possível executar a declaração SQL !'); </script>");
            }
        } catch (PDOException $erro) {
            return "Erro: " . $erro->getMessage();
        }
    }

    public function obterReservadosUltimoMes(){
        try {
            $statement = Conexao::getInstance()->prepare("SELECT COUNT(emp.id_reserva) total
                                                               , MONTHNAME(STR_TO_DATE(MONTH(now()), '%m')) mes
                                                            FROM tb_reserva emp
                                                           WHERE MONTH(emp.data_reserva) = MONTH(now())");
            if ($statement->execute()) {
                return $statement->fetch(PDO::FETCH_OBJ);
            } else {
                throw new PDOException("<script> alert('Não foi possível executar a declaração SQL !'); </script>");
            }
        } catch (PDOException $erro) {
            return "Erro: " . $erro->getMessage();
        }
    }

    public function salvar($source)
    {
        try {
            if ($source->getIdReserva() != "") {
                $statement = Conexao::getInstance()->prepare("UPDATE tb_reserva
                                                                 SET id_exemplar = :idExemplar
                                                                   , id_usuario = :idUsuario
                                                                   , id_usuario_cancela = :idUsuarioCancela
                                                                   , data_reserva = :dataReserva
                                                               WHERE id_reserva = :id");
                $statement->bindValue(":id", $source->getIdReserva());
            } else {
                $statement = Conexao::getInstance()->prepare("INSERT INTO tb_reserva (id_exemplar
                                                                                    , id_usuario
                                                                                    , id_usuario_cancela
                                                                                    , data_reserva)
                                                                               VALUES (:idExemplar
                                                                                    , :idUsuario
                                                                                    , :idUsuarioCancela
                                                                                    , :dataReserva)");
            }
            $statement->bindValue(":idExemplar", $source->getIdExemplar());
            $statement->bindValue(":idUsuario", $source->getIdUsuario());
            $statement->bindValue(":idUsuarioCancela", $source->getIdUsuarioCancela());
            $statement->bindValue(":dataReserva", $source->getDataReserva());
            if ($statement->execute()) {
                if ($statement->rowCount() > 0) {
                    return "<script> alert('Dados cadastrados com sucesso !'); </script>";
                } else {
                    return "<script> alert('Erro ao tentar efetivar cadastro !'); </script>";
                }
            } else {
                throw new PDOException("<script> alert('Não foi possível executar a declaração SQL !'); </script>");
            }
        } catch (PDOException $erro) {
            return "Erro: " . $erro->getMessage();
        }
    }

    public function atualizar($source)
    {
        try {
            $statement = Conexao::getInstance()->prepare("SELECT id_reserva, id_exemplar, id_usuario, id_usuario_cancela, data_reserva
                                                            FROM tb_reserva WHERE id_reserva = :id");
            $statement->bindValue(":id", $source->getIdReserva());
            if ($statement->execute()) {
                $rs = $statement->fetch(PDO::FETCH_OBJ);
                $source->setIdReserva($rs->id_reserva);
                $source->setIdExemplar($rs->id_exemplar);
                $source->setIdUsuario($rs->id_usuario);
                $source->setIdUsuarioCancela($rs->id_usuario_cancela);
                $source->setDataReserva($rs->data_reserva);
                return $source;
            } else {
                throw new PDOException("<script> alert('Não foi possível executar a declaração SQL !'); </script>");
            }
        } catch (PDOException $erro) {
            return "Erro: " . $erro->getMessage();
        }
    }

    public function tabelapaginada()
    {
        //endereço atual da página
        $endereco = $_SERVER ['PHP_SELF'];
        /* Constantes de configuração */
        define('QTDE_REGISTROS', 2);
        define('RANGE_PAGINAS', 3);
        /* Recebe o número da página via parâmetro na URL */
        $pagina_atual = (isset($_GET['page']) && is_numeric($_GET['page'])) ? $_GET['page'] : 1;
        /* Calcula a linha inicial da consulta */
        $linha_inicial = ($pagina_atual - 1) * QTDE_REGISTROS;
        /* Instrução de consulta para paginação com MySQL */
        $sql = "SELECT re.id_reserva, re.data_reserva, li.titulo, usu.nomeUsuario
                  FROM tb_reserva re 
                 INNER JOIN tb_exemplar ex
                    ON re.id_exemplar = ex.idtb_exemplar
                 INNER JOIN tb_livro li
                    ON li.idtb_livro = ex.tb_livro_idtb_livro
                 INNER JOIN tb_usuario usu
                    ON usu.idtb_usuario = re.id_usuario LIMIT {$linha_inicial}, " . QTDE_REGISTROS;
        $statement = Conexao::getInstance()->prepare($sql);
        $statement->execute();
        $dados = $statement->fetchAll(PDO::FETCH_OBJ);
        /* Conta quantos registos existem na tabela */
        $sqlContador = "SELECT COUNT(*) AS total_registros FROM tb_autores";
        $statement = Conexao::getInstance()->prepare($sqlContador);
        $statement->execute();
        $valor = $statement->fetch(PDO::FETCH_OBJ);
        /* Idêntifica a primeira página */
        $primeira_pagina = 1;
        /* Cálcula qual será a última página */
        $ultima_pagina = ceil($valor->total_registros / QTDE_REGISTROS);
        /* Cálcula qual será a página anterior em relação a página atual em exibição */
        $pagina_anterior = ($pagina_atual > 1) ? $pagina_atual - 1 : 0;
        /* Cálcula qual será a pŕoxima página em relação a página atual em exibição */
        $proxima_pagina = ($pagina_atual < $ultima_pagina) ? $pagina_atual + 1 : 0;
        /* Cálcula qual será a página inicial do nosso range */
        $range_inicial = (($pagina_atual - RANGE_PAGINAS) >= 1) ? $pagina_atual - RANGE_PAGINAS : 1;
        /* Cálcula qual será a página final do nosso range */
        $range_final = (($pagina_atual + RANGE_PAGINAS) <= $ultima_pagina) ? $pagina_atual + RANGE_PAGINAS : $ultima_pagina;
        /* Verifica se vai exibir o botão "Primeiro" e "Pŕoximo" */
        $exibir_botao_inicio = ($range_inicial < $pagina_atual) ? '' : 'hidden';
        /* Verifica se vai exibir o botão "Anterior" e "Último" */
        $exibir_botao_final = ($range_final > $pagina_atual) ? '' : 'hidden';
        if (!empty($dados)):
            echo "
     <table class='table table-striped table-bordered'>
     <thead>
       <tr style='text-transform: uppercase;' class='active'>
        <th style='text-align: center; font-weight: bolder;'>ID</th>
        <th style='text-align: center; font-weight: bolder;'>Exemplar</th>
        <th style='text-align: center; font-weight: bolder;'>Usuário</th>
        <th style='text-align: center; font-weight: bolder;'>Data da reserva</th>
        <th style='text-align: center; font-weight: bolder;'>Cancelar reserva</th>
       </tr>
     </thead>
     <tbody>";
            foreach ($dados as $source):
                echo "<tr>
        <td style='text-align: center'>$source->id_reserva</td>
        <td style='text-align: center'>$source->titulo</td>
        <td style='text-align: center'>$source->nomeUsuario</td>
        <td style='text-align: center'>$source->data_reserva</td>
        <td style='text-align: center'><a href='?act=del&id=$source->id_reserva' title='Remover'><i class='ti-close'></i></a></td>
       </tr>";
            endforeach;
            echo "
</tbody>
    </table>
     <div class='box-paginacao' style='text-align: center'>
       <a class='box-navegacao  $exibir_botao_inicio' href='$endereco?page=$primeira_pagina' title='Primeira Página'> Primeira  |</a>
       <a class='box-navegacao  $exibir_botao_inicio' href='$endereco?page=$pagina_anterior' title='Página Anterior'> Anterior  |</a>
";
            /* Loop para montar a páginação central com os números */
            for ($i = $range_inicial; $i <= $range_final; $i++):
                $destaque = ($i == $pagina_atual) ? 'destaque' : '';
                echo "<a class='box-numero $destaque' href='$endereco?page=$i'> ( $i ) </a>";
            endfor;
            echo "<a class='box-navegacao $exibir_botao_final' href='$endereco?page=$proxima_pagina' title='Próxima Página'>| Próxima  </a>
                  <a class='box-navegacao $exibir_botao_final' href='$endereco?page=$ultima_pagina'  title='Última Página'>| Última  </a>
     </div>";
        else:
            echo "<p class='bg-danger'>Nenhum registro foi encontrado!</p>
     ";
        endif;
    }

}