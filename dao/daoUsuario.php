<?php

require_once "iPage.php";

class daoUsuario implements iPage
{

    public function logar($login, $senha){
        try {
            $statement = Conexao::getInstance()->prepare("SELECT * FROM tb_usuario WHERE nomeUsuario = :nome AND senha = :senha");
            $statement->bindValue(":nome", $login);
            $statement->bindValue(":senha", $senha);
            if ($statement->execute()) {
                if($statement->rowCount() > 0)
                {
                    $rs = $statement->fetch(PDO::FETCH_OBJ);
                    return $rs;
                }
                return null;
            } else {
                throw new PDOException("<script> alert('Não foi possível executar a declaração SQL !'); </script>");
            }
        } catch (PDOException $erro) {
            return "Erro: " . $erro->getMessage();
        }
    }

    public function verificarAlterarSenha($id){
        try {
            $statement = Conexao::getInstance()->prepare("SELECT redefinir_senha FROM tb_usuario WHERE idtb_usuario = :id");
            $statement->bindValue(":id", $id);
            if ($statement->execute()) {
                if($statement->rowCount() > 0)
                {
                    $rs = $statement->fetch(PDO::FETCH_OBJ);
                    if($rs->redefinir_senha == 'S')
                        return true;
                    else
                        return false;
                }
            } else {
                throw new PDOException("<script> alert('Não foi possível executar a declaração SQL !'); </script>");
            }
        } catch (PDOException $erro) {
            return "Erro: " . $erro->getMessage();
        }
    }

    public function alterarSenha($id, $senha){
        try {
            $st = Conexao::getInstance()->prepare("UPDATE tb_usuario SET senha = :senha, redefinir_senha = 'N' WHERE idtb_usuario = :id AND redefinir_senha = 'S'");
            $st->bindValue(":id", $id);
            $st->bindValue(":senha", $senha);
            $st->execute();
            return true;
        }
        catch (PDOException $erro) {
            return "Erro: " . $erro->getMessage();
        }
    }

    public function recuperarSenha($email){
        try {
            $statement = Conexao::getInstance()->prepare("SELECT * FROM tb_usuario WHERE email = :email");
            $statement->bindValue(":email", $email);
            if ($statement->execute()) {
                if($statement->rowCount() > 0)
                {
                    $rs = $statement->fetch(PDO::FETCH_OBJ);

                    $st = Conexao::getInstance()->prepare("UPDATE tb_usuario SET redefinir_senha = 'S' WHERE email = :email");
                    $st->bindValue(":email", $email);
                    $st->execute();
                    
                    $destino = $email;
                    $assunto = "Recuperar Senha";
                
                    $headers  = 'MIME-Version: 1.0' . "\r\n";
                    $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                    $headers .= 'From: Biblioteca CES <biblioteca@biblioteca.com>';
                    
                    $content = "Para alterar sua senha <a href='http://localhost/trabalhobiblioteca/alterarSenha.php?token=$rs->idtb_usuario'>Clique Aqui</a>";

                    $enviaremail = mail($destino, $assunto, $content, $headers);
                    return true;
                }
                return null;
            } else {
                throw new PDOException("<script> alert('Não foi possível executar a declaração SQL !'); </script>");
            }

            
        } catch (PDOException $erro) {
            return "Erro: " . $erro->getMessage();
        }
    }

    public function remover($source)
    {
        try {
            $statement = Conexao::getInstance()->prepare("DELETE FROM tb_usuario WHERE idtb_usuario = :id");
            $statement->bindValue(":id", $source->getIdtbUsuario());
            if ($statement->execute()) {
                return "<script> alert('Registo foi excluído com êxito !'); </script>";
            } else {
                throw new PDOException("<script> alert('Não foi possível executar a declaração SQL !'); </script>");
            }
        } catch (PDOException $erro) {
            return "Erro: " . $erro->getMessage();
        }
    }

    public function salvar($source)
    {
        try {
            if ($source->getIdtbUsuario() != "") {
                $statement = Conexao::getInstance()->prepare("UPDATE tb_usuario 
                                                                 SET nomeUsuario = :nome
                                                                   , tipo = :tipo
                                                                   , senha = :senha
                                                                   , email = :email
                                                               WHERE idtb_usuario = :id;");
                $statement->bindValue(":id", $source->getIdtbUsuario());
            } else {
                $statement = Conexao::getInstance()->prepare("INSERT 
                                                                INTO tb_usuario (nomeUsuario
                                                                                , tipo
                                                                                , senha
                                                                                , email) 
                                                              VALUES (:nome
                                                                     , :tipo
                                                                     , :senha
                                                                     , :email)");
            }
            $statement->bindValue(":nome", $source->getNomeUsuario());
            $statement->bindValue(":tipo", $source->getTipo());
            $statement->bindValue(":senha", $source->getSenha());
            $statement->bindValue(":email", $source->getEmail());
            if ($statement->execute()) {
                if ($statement->rowCount() > 0) {
                    return "<script> alert('Dados cadastrados com sucesso !'); </script>";
                } else {
                    return "<script> alert('Erro ao tentar efetivar cadastro !'); </script>";
                }
            } else {
                throw new PDOException("<script> alert('Não foi possível executar a declaração SQL !'); </script>");
            }
        } catch (PDOException $erro) {
            return "Erro: " . $erro->getMessage();
        }
    }

    public function getComboBoxUsuario(){
        try {
            $statement = Conexao::getInstance()->prepare("SELECT idtb_usuario
                                                               , nomeUsuario
                                                            FROM tb_usuario");
            if ($statement->execute()) {
                return $statement->fetchAll(PDO::FETCH_OBJ);
            } else {
                throw new PDOException("<script> alert('Não foi possível executar a declaração SQL !'); </script>");
            }
        } catch (PDOException $erro) {
            return "Erro: " . $erro->getMessage();
        }
    }

    public function atualizar($source)
    {
        try {
            $statement = Conexao::getInstance()->prepare("SELECT idtb_usuario
                                                               , nomeUsuario
                                                               , tipo
                                                               , senha
                                                               , email
                                                            FROM tb_usuario
                                                           WHERE idtb_usuario = :id");
            $statement->bindValue(":id", $source->getIdtbUsuario());
            if ($statement->execute()) {
                $rs = $statement->fetch(PDO::FETCH_OBJ);
                $source->setIdtbUsuario($rs->idtb_usuario);
                $source->setNomeUsuario($rs->nomeUsuario);
                $source->setTipo($rs->tipo);
                $source->setSenha($rs->senha);
                $source->setEmail($rs->email);
                return $source;
            } else {
                throw new PDOException("<script> alert('Não foi possível executar a declaração SQL !'); </script>");
            }
        } catch (PDOException $erro) {
            return "Erro: " . $erro->getMessage();
        }
    }

    public function tabelapaginada()
    {
        $cargo = [
            1 => 'Aluno',
            3 => 'Bibliotecario',
            8 => 'Professor',
            10 => 'Admin'
        ];

        //endereço atual da página
        $endereco = $_SERVER ['PHP_SELF'];
        /* Constantes de configuração */
        define('QTDE_REGISTROS', 2);
        define('RANGE_PAGINAS', 3);
        /* Recebe o número da página via parâmetro na URL */
        $pagina_atual = (isset($_GET['page']) && is_numeric($_GET['page'])) ? $_GET['page'] : 1;
        /* Calcula a linha inicial da consulta */
        $linha_inicial = ($pagina_atual - 1) * QTDE_REGISTROS;
        /* Instrução de consulta para paginação com MySQL */
        $sql = "SELECT idtb_usuario
                     , nomeUsuario
                     , tipo
                     , senha
                     , email
                  FROM tb_usuario
                 LIMIT {$linha_inicial}, " . QTDE_REGISTROS;
        $statement = Conexao::getInstance()->prepare($sql);
        $statement->execute();
        $dados = $statement->fetchAll(PDO::FETCH_OBJ);
        /* Conta quantos registos existem na tabela */
        $sqlContador = "SELECT COUNT(*) AS total_registros FROM tb_usuario";
        $statement = Conexao::getInstance()->prepare($sqlContador);
        $statement->execute();
        $valor = $statement->fetch(PDO::FETCH_OBJ);
        /* Idêntifica a primeira página */
        $primeira_pagina = 1;
        /* Cálcula qual será a última página */
        $ultima_pagina = ceil($valor->total_registros / QTDE_REGISTROS);
        /* Cálcula qual será a página anterior em relação a página atual em exibição */
        $pagina_anterior = ($pagina_atual > 1) ? $pagina_atual - 1 : 0;
        /* Cálcula qual será a pŕoxima página em relação a página atual em exibição */
        $proxima_pagina = ($pagina_atual < $ultima_pagina) ? $pagina_atual + 1 : 0;
        /* Cálcula qual será a página inicial do nosso range */
        $range_inicial = (($pagina_atual - RANGE_PAGINAS) >= 1) ? $pagina_atual - RANGE_PAGINAS : 1;
        /* Cálcula qual será a página final do nosso range */
        $range_final = (($pagina_atual + RANGE_PAGINAS) <= $ultima_pagina) ? $pagina_atual + RANGE_PAGINAS : $ultima_pagina;
        /* Verifica se vai exibir o botão "Primeiro" e "Pŕoximo" */
        $exibir_botao_inicio = ($range_inicial < $pagina_atual) ? '' : 'hidden';
        /* Verifica se vai exibir o botão "Anterior" e "Último" */
        $exibir_botao_final = ($range_final > $pagina_atual) ? '' : 'hidden';
        if (!empty($dados)):
            echo "
     <table class='table table-striped table-bordered'>
     <thead>
       <tr style='text-transform: uppercase;' class='active'>
        <th style='text-align: center; font-weight: bolder;'>ID</th>
        <th style='text-align: center; font-weight: bolder;'>Nome</th>
        <th style='text-align: center; font-weight: bolder;'>Tipo</th>
        <th style='text-align: center; font-weight: bolder;'>Email</th>
        <th style='text-align: center; font-weight: bolder;' colspan='2'>Ações</th>
       </tr>
     </thead>
     <tbody>";
            foreach ($dados as $source):
                $tipo = $cargo[$source->tipo];
                echo "<tr>
        <td style='text-align: center'>$source->idtb_usuario</td>
        <td style='text-align: center'>$source->nomeUsuario</td>
        <td style='text-align: center'>$tipo</td>
        <td style='text-align: center'>$source->email</td>
        <td style='text-align: center'><a href='?act=upd&id=$source->idtb_usuario' title='Alterar'><i class='ti-reload'></i></a></td>
        <td style='text-align: center'><a href='?act=del&id=$source->idtb_usuario' title='Remover'><i class='ti-close'></i></a></td>
       </tr>";
            endforeach;
            echo "
</tbody>
    </table>
     <div class='box-paginacao' style='text-align: center'>
       <a class='box-navegacao  $exibir_botao_inicio' href='$endereco?page=$primeira_pagina' title='Primeira Página'> Primeira  |</a>
       <a class='box-navegacao  $exibir_botao_inicio' href='$endereco?page=$pagina_anterior' title='Página Anterior'> Anterior  |</a>
";
            /* Loop para montar a páginação central com os números */
            for ($i = $range_inicial; $i <= $range_final; $i++):
                $destaque = ($i == $pagina_atual) ? 'destaque' : '';
                echo "<a class='box-numero $destaque' href='$endereco?page=$i'> ( $i ) </a>";
            endfor;
            echo "<a class='box-navegacao $exibir_botao_final' href='$endereco?page=$proxima_pagina' title='Próxima Página'>| Próxima  </a>
                  <a class='box-navegacao $exibir_botao_final' href='$endereco?page=$ultima_pagina'  title='Última Página'>| Última  </a>
     </div>";
        else:
            echo "<p class='bg-danger'>Nenhum registro foi encontrado!</p>
     ";
        endif;
    }

}