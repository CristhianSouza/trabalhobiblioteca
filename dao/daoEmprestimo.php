<?php

require_once "iPage.php";

class daoEmprestimo implements iPage
{

    public function remover($source)
    {
        try {
            $statement = Conexao::getInstance()->prepare("UPDATE tb_emprestimo SET ic_emprestado = 0 WHERE idtb_emprestimo = :idEmprestimo");
            $statement->bindValue(":idEmprestimo", $source);
            if ($statement->execute()) {
                return "<script> alert('Exemplar devolvido com sucesso!'); </script>";
            } else {
                throw new PDOException("<script> alert('Não foi possível executar a declaração SQL !'); </script>");
            }
        } catch (PDOException $erro) {
            return "Erro: " . $erro->getMessage();
        }
    }

    public function obterEmprestadosUltimoMes(){
        try {
            $statement = Conexao::getInstance()->prepare("SELECT COUNT(emp.idtb_emprestimo) total
                                                               , MONTHNAME(STR_TO_DATE(MONTH(now()), '%m')) mes
                                                            FROM tb_emprestimo emp
                                                           WHERE MONTH(emp.dataEmprestimo) = MONTH(now())");
            if ($statement->execute()) {
                return $statement->fetch(PDO::FETCH_OBJ);
            } else {
                throw new PDOException("<script> alert('Não foi possível executar a declaração SQL !'); </script>");
            }
        } catch (PDOException $erro) {
            return "Erro: " . $erro->getMessage();
        }
    }

    public function obterEmprestadosPenultimoMes(){
        try {
            $statement = Conexao::getInstance()->prepare("SELECT COUNT(emp.idtb_emprestimo) total
                                                               , MONTHNAME(STR_TO_DATE(MONTH(now())-1, '%m')) mes
                                                            FROM tb_emprestimo emp
                                                           WHERE MONTH(emp.dataEmprestimo) = MONTH(now())-1");
            if ($statement->execute()) {
                return $statement->fetch(PDO::FETCH_OBJ);
            } else {
                throw new PDOException("<script> alert('Não foi possível executar a declaração SQL !'); </script>");
            }
        } catch (PDOException $erro) {
            return "Erro: " . $erro->getMessage();
        }
    }

    public function obterEmprestadosAntePenultimoMes(){
        try {
            $statement = Conexao::getInstance()->prepare("SELECT COUNT(emp.idtb_emprestimo) total
                                                               , MONTHNAME(STR_TO_DATE(MONTH(now())-2, '%m')) mes
                                                            FROM tb_emprestimo emp
                                                           WHERE MONTH(emp.dataEmprestimo) = MONTH(now())-2");
            if ($statement->execute()) {
                return $statement->fetch(PDO::FETCH_OBJ);;
            } else {
                throw new PDOException("<script> alert('Não foi possível executar a declaração SQL !'); </script>");
            }
        } catch (PDOException $erro) {
            return "Erro: " . $erro->getMessage();
        }
    }

    public function obterEmprestadosPorCategoria(){
        try {
            $statement = Conexao::getInstance()->prepare("SELECT COUNT(emp.idtb_emprestimo) total
                                                               , cat.nomeCategoria
                                                            FROM tb_categoria cat
                                                            LEFT JOIN tb_livro liv
                                                              ON liv.tb_categoria_idtb_categoria = cat.idtb_categoria
                                                            LEFT JOIN tb_exemplar exe
                                                              ON exe.tb_livro_idtb_livro = liv.idtb_livro
                                                            LEFT JOIN tb_emprestimo emp
                                                              ON emp.tb_exemplar_idtb_exemplar = exe.idtb_exemplar    
                                                           WHERE MONTH(emp.dataEmprestimo) > MONTH(now())-3
                                                              OR emp.dataEmprestimo IS NULL
                                                           GROUP BY cat.idtb_categoria");
            if ($statement->execute()) {
                return $statement->fetchAll(PDO::FETCH_OBJ);
            } else {
                throw new PDOException("<script> alert('Não foi possível executar a declaração SQL !'); </script>");
            }
        } catch (PDOException $erro) {
            return "Erro: " . $erro->getMessage();
        }
    }

    public function salvar($source)
    {
        try {
            $statement = Conexao::getInstance()->prepare("INSERT INTO tb_emprestimo 
                (tb_usuaio_idtb_usuaio, tb_exemplar_idtb_exemplar, dataEmprestimo, observacao) 
                VALUES (:tb_usuaio_idtb_usuaio, :tb_exemplar_idtb_exemplar, :dataEmprestimo, :observacao)");

            $statement->bindValue(":tb_usuaio_idtb_usuaio", $source->getUsuario());
            $statement->bindValue(":tb_exemplar_idtb_exemplar", $source->getExemplar());
            $statement->bindValue(":dataEmprestimo", $source->getDataEmprestimo());
            $statement->bindValue(":observacao", $source->getObservacao());
            if ($statement->execute()) {
                if ($statement->rowCount() > 0) {
                    return "<script> alert('Dados cadastrados com sucesso !'); </script>";
                } else {
                    return "<script> alert('Erro ao tentar efetivar cadastro !'); </script>";
                }
            } else {
                throw new PDOException("<script> alert('Não foi possível executar a declaração SQL !'); </script>");
            }
        } catch (PDOException $erro) {
            return "Erro: " . $erro->getMessage();
        }
    }

    public function verificarDebito($idUsuario)
    {
        try {
            $statement = Conexao::getInstance()->prepare("SELECT COUNT(*) total
                                                            FROM tb_emprestimo
                                                           WHERE sysdate() > ADDDATE(dataEmprestimo, INTERVAL 3 DAY)
                                                             AND tb_usuaio_idtb_usuaio = :id
                                                             AND ic_emprestado = 1");
            $statement->bindValue(":id", $idUsuario);
            if ($statement->execute()) {
                $rs = $statement->fetch(PDO::FETCH_OBJ);
                return $rs->total > 0;
            } else {
                throw new PDOException("<script> alert('Não foi possível executar a declaração SQL !'); </script>");
            }
        } catch (PDOException $erro) {
            return "Erro: " . $erro->getMessage();
        }
    }

    public function verificarQuantidadeEmprestadaMaxima($idUsuario)
    {
        try {
                $statement = Conexao::getInstance()->prepare("SELECT COUNT(emp.idtb_emprestimo) total
                                                                   , usu.tipo
                                                                FROM tb_emprestimo emp
                                                               INNER JOIN tb_usuario usu
                                                                  ON emp.tb_usuaio_idtb_usuaio = usu.idtb_usuario
                                                               WHERE emp.tb_usuaio_idtb_usuaio = :id
                                                                 AND emp.ic_emprestado = 1");
            $statement->bindValue(":id", $idUsuario);
            if ($statement->execute()) {
                if ($statement->rowCount() > 0) {
                    $rs = $statement->fetch(PDO::FETCH_OBJ);
                    if($rs->tipo == 8)
                        return $rs->total == 5;
                    else
                        return $rs->total == 3;
                } else {
                    return false;
                }
            } else {
                throw new PDOException("<script> alert('Não foi possível executar a declaração SQL !'); </script>");
            }
        } catch (PDOException $erro) {
            return "Erro: " . $erro->getMessage();
        }
    }

    public function atualizar($source)
    {
        try {
            $statement = Conexao::getInstance()->prepare(" SELECT emp.idtb_emprestimo
                                                                  emp.tb_usuaio_idtb_usuaio,
                                                                  emp.tb_exemplar_idtb_exemplar
                                                                  emp.dataEmprestimo,
                                                                  emp.observacao,
                                                                  usu.tipo,
                                                                  usu.email,
                                                                  usu.nomeUsuario,
                                                                  liv.idtb_livro
                                                                  liv.titulo,
                                                                  liv.isbn,
                                                                  liv.edicao,
                                                                  liv.ano,
                                                                  liv.upload,
                                                                  exe.tipoExemplar
                                                             FROM tb_emprestimo emp
                                                            INNER JOIN tb_usuario usu
                                                               ON usu.idtb_usuario = emp.tb_usuaio_idtb_usuaio
                                                            INNER JOIN tb_exemplar exe
                                                               ON emp.tb_exemplar_idtb_exemplar = exe.idtb_exemplar
                                                            INNER JOIN tb_livro liv
                                                               ON liv.idtb_livro = exe.tb_livro_idtb_livro
                                                            WHERE tb_usuaio_idtb_usuaio = :usuario
                                                              AND tb_exemplar_idtb_exemplar = :exemplar");
            $statement->bindValue(":usuario", $source->getUsuario());
            $statement->bindValue(":exemplar", $source->getExemplar());
            if ($statement->execute()) {
                $rs = $statement->fetch(PDO::FETCH_OBJ);
                $usuario = new usuario($rs->tb_usuaio_idtb_usuaio, $rs->nomeUsuario, $rs->tipo, null, $rs->email);
                $source->setUsuario($usuario);
                $exemplar = new exemplar($rs->tb_exemplar_idtb_exemplar, $rs->idtb_livro, $rs->tipoExemplar);
                $source->setExemplar($exemplar);
                $source->setDataEmprestimo($rs->dataEmprestimo);
                $source->setObservacao($rs->observacao);
                return $source;
            } else {
                throw new PDOException("<script> alert('Não foi possível executar a declaração SQL !'); </script>");
            }
        } catch (PDOException $erro) {
            return "Erro: " . $erro->getMessage();
        }
    }

    public function tabelapaginada()
    {
        //endereço atual da página
        $endereco = $_SERVER ['PHP_SELF'];
        /* Constantes de configuração */
        define('QTDE_REGISTROS', 2);
        define('RANGE_PAGINAS', 3);
        /* Recebe o número da página via parâmetro na URL */
        $pagina_atual = (isset($_GET['page']) && is_numeric($_GET['page'])) ? $_GET['page'] : 1;
        /* Calcula a linha inicial da consulta */
        $linha_inicial = ($pagina_atual - 1) * QTDE_REGISTROS;
        /* Instrução de consulta para paginação com MySQL */
        $sql = "SELECT emp.idtb_emprestimo,
                       emp.tb_usuaio_idtb_usuaio,
                       emp.tb_exemplar_idtb_exemplar,
                       emp.dataEmprestimo,
                       emp.observacao,
                       usu.nomeUsuario,
                       liv.titulo,
                       ADDDATE(emp.dataEmprestimo, INTERVAL 3 DAY) devolucao
                  FROM tb_emprestimo emp
                 INNER JOIN tb_usuario usu
                    ON usu.idtb_usuario = emp.tb_usuaio_idtb_usuaio
                 INNER JOIN tb_exemplar exe
                    ON emp.tb_exemplar_idtb_exemplar = exe.idtb_exemplar
                 INNER JOIN tb_livro liv
                    ON liv.idtb_livro = exe.tb_livro_idtb_livro
                 WHERE emp.ic_emprestado = 1
                 LIMIT {$linha_inicial}, " . QTDE_REGISTROS;
        $statement = Conexao::getInstance()->prepare($sql);
        $statement->execute();
        $dados = $statement->fetchAll(PDO::FETCH_OBJ);
        /* Conta quantos registos existem na tabela */
        $sqlContador = "SELECT COUNT(*) AS total_registros FROM tb_emprestimo";
        $statement = Conexao::getInstance()->prepare($sqlContador);
        $statement->execute();
        $valor = $statement->fetch(PDO::FETCH_OBJ);
        /* Idêntifica a primeira página */
        $primeira_pagina = 1;
        /* Cálcula qual será a última página */
        $ultima_pagina = ceil($valor->total_registros / QTDE_REGISTROS);
        /* Cálcula qual será a página anterior em relação a página atual em exibição */
        $pagina_anterior = ($pagina_atual > 1) ? $pagina_atual - 1 : 0;
        /* Cálcula qual será a pŕoxima página em relação a página atual em exibição */
        $proxima_pagina = ($pagina_atual < $ultima_pagina) ? $pagina_atual + 1 : 0;
        /* Cálcula qual será a página inicial do nosso range */
        $range_inicial = (($pagina_atual - RANGE_PAGINAS) >= 1) ? $pagina_atual - RANGE_PAGINAS : 1;
        /* Cálcula qual será a página final do nosso range */
        $range_final = (($pagina_atual + RANGE_PAGINAS) <= $ultima_pagina) ? $pagina_atual + RANGE_PAGINAS : $ultima_pagina;
        /* Verifica se vai exibir o botão "Primeiro" e "Pŕoximo" */
        $exibir_botao_inicio = ($range_inicial < $pagina_atual) ? '' : 'hidden';
        /* Verifica se vai exibir o botão "Anterior" e "Último" */
        $exibir_botao_final = ($range_final > $pagina_atual) ? '' : 'hidden';
        if (!empty($dados)):
            echo "
     <table class='table table-striped table-bordered'>
     <thead>
       <tr style='text-transform: uppercase;' class='active'>
        <th style='text-align: center; font-weight: bolder;'>Livro</th>
        <th style='text-align: center; font-weight: bolder;'>Emprestado para</th>
        <th style='text-align: center; font-weight: bolder;'>Data de empréstimo</th>
        <th style='text-align: center; font-weight: bolder;'>Observação</th>
        <th style='text-align: center; font-weight: bolder;'>Devolução prevista</th>
        <th style='text-align: center; font-weight: bolder;'>Ações</th>
       </tr>
     </thead>
     <tbody>";
            foreach ($dados as $source):
                echo "<tr>
        <td style='text-align: center'>$source->titulo</td>
        <td style='text-align: center'>$source->nomeUsuario</td>
        <td style='text-align: center'>$source->dataEmprestimo</td>
        <td style='text-align: center'>$source->observacao</td>
        <td style='text-align: center'>$source->devolucao</td>
        <td style='text-align: center'><a href='?act=del&idEmprestimo=$source->idtb_emprestimo' title='Realizar devolução'><i class='ti-close'></i></a></td>
       </tr>";
            endforeach;
            echo "
</tbody>
    </table>
     <div class='box-paginacao' style='text-align: center'>
       <a class='box-navegacao  $exibir_botao_inicio' href='$endereco?page=$primeira_pagina' title='Primeira Página'> Primeira  |</a>
       <a class='box-navegacao  $exibir_botao_inicio' href='$endereco?page=$pagina_anterior' title='Página Anterior'> Anterior  |</a>
";
            /* Loop para montar a páginação central com os números */
            for ($i = $range_inicial; $i <= $range_final; $i++):
                $destaque = ($i == $pagina_atual) ? 'destaque' : '';
                echo "<a class='box-numero $destaque' href='$endereco?page=$i'> ( $i ) </a>";
            endfor;
            echo "<a class='box-navegacao $exibir_botao_final' href='$endereco?page=$proxima_pagina' title='Próxima Página'>| Próxima  </a>
                  <a class='box-navegacao $exibir_botao_final' href='$endereco?page=$ultima_pagina'  title='Última Página'>| Última  </a>
     </div>";
        else:
            echo "<p class='bg-danger'>Nenhum registro foi encontrado!</p>
     ";
        endif;
    }

}