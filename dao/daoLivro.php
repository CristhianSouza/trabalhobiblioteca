<?php

require_once "iPage.php";
require_once "modelo/categoria.php";
require_once "modelo/editora.php";

class daoLivro implements iPage
{

    public function remover($source)
    {
        try {
            $statement = Conexao::getInstance()->prepare("DELETE FROM tb_livro WHERE idtb_livro = :id");
            $statement->bindValue(":id", $source->getIdLivro());
            if ($statement->execute()) {
                return "<script> alert('Registo foi excluído com êxito !'); </script>";
            } else {
                throw new PDOException("<script> alert('Não foi possível executar a declaração SQL !'); </script>");
            }
        } catch (PDOException $erro) {
            return "Erro: " . $erro->getMessage();
        }
    }

    public function salvar($source)
    {
        try {
            if ($source->getIdLivro() != "") {
                $statement = Conexao::getInstance()->prepare("UPDATE tb_livro 
                                                                 SET titulo = :titulo
                                                                   , isbn = :isbn
                                                                   , edicao = :edicao
                                                                   , ano = :ano
                                                                   , upload = :upload
                                                                   , tb_editora_idtb_editora = :tb_editora_idtb_editora
                                                                   , tb_categoria_idtb_categoria = :tb_categoria_idtb_categoria
                                                               WHERE idtb_livro = :id;");
                $statement->bindValue(":id", $source->getIdLivro());
            } else {
                $statement = Conexao::getInstance()->prepare("INSERT 
                                                                INTO tb_livro (titulo
                                                                            , isbn
                                                                            , edicao
                                                                            , ano
                                                                            , upload
                                                                            , tb_editora_idtb_editora
                                                                            , tb_categoria_idtb_categoria) 
                                                              VALUES (:titulo
                                                                     , :isbn
                                                                     , :edicao
                                                                     , :ano
                                                                     , :upload
                                                                     , :tb_editora_idtb_editora
                                                                     , :tb_categoria_idtb_categoria)");
            }
            $statement->bindValue(":titulo", $source->getTitulo());
            $statement->bindValue(":isbn", $source->getIsbn());
            $statement->bindValue(":edicao", $source->getEdicao());
            $statement->bindValue(":ano", $source->getAno());
            $statement->bindValue(":upload", $source->getUpload());
            $statement->bindValue(":tb_editora_idtb_editora", $source->getEditora());
            $statement->bindValue(":tb_categoria_idtb_categoria", $source->getCategoria());
            if ($statement->execute()) {
                if ($statement->rowCount() > 0) {
                    return "<script> alert('Dados cadastrados com sucesso !'); </script>";
                } else {
                    return "<script> alert('Erro ao tentar efetivar cadastro !'); </script>";
                }
            } else {
                throw new PDOException("<script> alert('Não foi possível executar a declaração SQL !'); </script>");
            }
        } catch (PDOException $erro) {
            return "Erro: " . $erro->getMessage();
        }
    }

    public function atualizar($source)
    {
        try {
            $statement = Conexao::getInstance()->prepare("SELECT livro.idtb_livro
                                                               , livro.titulo
                                                               , livro.isbn
                                                               , livro.edicao
                                                               , livro.ano
                                                               , livro.upload
                                                               , editora.idtb_editora
                                                               , editora.nomeEditora
                                                               , categoria.idtb_categoria
                                                               , categoria.nomeCategoria
                                                            FROM tb_livro livro
                                                            LEFT JOIN tb_editora editora
                                                              ON editora.idtb_editora = livro.tb_editora_idtb_editora
                                                            LEFT JOIN tb_categoria categoria
                                                              ON categoria.idtb_categoria = livro.tb_categoria_idtb_categoria
                                                           WHERE idtb_livro = :id");
            $statement->bindValue(":id", $source->getIdLivro());
            if ($statement->execute()) {
                $rs = $statement->fetch(PDO::FETCH_OBJ);
                $source->setIdLivro($rs->idtb_livro);
                $source->setTitulo($rs->titulo);
                $source->setIsbn($rs->isbn);
                $source->setEdicao($rs->edicao);
                $source->setAno($rs->ano);
                $source->setUpload($rs->upload);
                $editora = new editora($rs->idtb_editora, $rs->nomeEditora);
                $source->setEditora($editora);
                $categoria = new categoria($rs->idtb_categoria, $rs->nomeCategoria);
                $source->setCategoria($categoria);
                return $source;
            } else {
                throw new PDOException("<script> alert('Não foi possível executar a declaração SQL !'); </script>");
            }
        } catch (PDOException $erro) {
            return "Erro: " . $erro->getMessage();
        }
    }

    public function getComboBoxLivro(){
        try {
            $statement = Conexao::getInstance()->prepare("SELECT idtb_livro, titulo FROM tb_livro");
            if ($statement->execute()) {
                return $statement->fetchAll(PDO::FETCH_OBJ);
            } else {
                throw new PDOException("<script> alert('Não foi possível executar a declaração SQL !'); </script>");
            }
        } catch (PDOException $erro) {
            return "Erro: " . $erro->getMessage();
        }
    }

    public function gerarRelatorioLivros($dataDe, $dataAte){
        try {
            $statement = Conexao::getInstance()->prepare("SELECT livro.titulo Titulo
                                                               , COUNT(emprestimo.tb_exemplar_idtb_exemplar) Emprestimos
                                                               , (SELECT COUNT(*)
                                                                    FROM tb_exemplar ex
                                                                   WHERE ex.tb_livro_idtb_livro = livro.idtb_livro) Exemplares
                                                            FROM tb_emprestimo emprestimo
                                                           INNER JOIN tb_exemplar exemplar
                                                              ON exemplar.idtb_exemplar = emprestimo.tb_exemplar_idtb_exemplar
                                                           INNER JOIN tb_livro livro
                                                              ON exemplar.tb_livro_idtb_livro = livro.idtb_livro
                                                           WHERE emprestimo.dataEmprestimo BETWEEN :dataDe  AND :dataAte
                                                           GROUP BY livro.idtb_livro");
            $statement->bindValue(":dataDe", $dataDe);
            $statement->bindValue(":dataAte", $dataAte);
            if ($statement->execute()) {
                return $statement->fetchAll(PDO::FETCH_OBJ);
            } else {
                throw new PDOException("<script> alert('Não foi possível executar a declaração SQL !'); </script>");
            }
        } catch (PDOException $erro) {
            return "Erro: " . $erro->getMessage();
        }
    }

    public function tabelapaginada()
    {
        //endereço atual da página
        $endereco = $_SERVER ['PHP_SELF'];
        /* Constantes de configuração */
        define('QTDE_REGISTROS', 2);
        define('RANGE_PAGINAS', 3);
        /* Recebe o número da página via parâmetro na URL */
        $pagina_atual = (isset($_GET['page']) && is_numeric($_GET['page'])) ? $_GET['page'] : 1;
        /* Calcula a linha inicial da consulta */
        $linha_inicial = ($pagina_atual - 1) * QTDE_REGISTROS;
        /* Instrução de consulta para paginação com MySQL */
        $sql = "SELECT livro.idtb_livro
                     , livro.titulo
                     , livro.isbn
                     , livro.edicao
                     , livro.ano
                     , livro.upload
                     , editora.nomeEditora
                     , categoria.nomeCategoria
                 FROM tb_livro livro
                 LEFT JOIN tb_editora editora
                   ON editora.idtb_editora = livro.tb_editora_idtb_editora
                 LEFT JOIN tb_categoria categoria
                   ON categoria.idtb_categoria = livro.tb_categoria_idtb_categoria LIMIT {$linha_inicial}, " . QTDE_REGISTROS;
        $statement = Conexao::getInstance()->prepare($sql);
        $statement->execute();
        $dados = $statement->fetchAll(PDO::FETCH_OBJ);
        /* Conta quantos registos existem na tabela */
        $sqlContador = "SELECT COUNT(*) AS total_registros FROM tb_livro";
        $statement = Conexao::getInstance()->prepare($sqlContador);
        $statement->execute();
        $valor = $statement->fetch(PDO::FETCH_OBJ);
        /* Idêntifica a primeira página */
        $primeira_pagina = 1;
        /* Cálcula qual será a última página */
        $ultima_pagina = ceil($valor->total_registros / QTDE_REGISTROS);
        /* Cálcula qual será a página anterior em relação a página atual em exibição */
        $pagina_anterior = ($pagina_atual > 1) ? $pagina_atual - 1 : 0;
        /* Cálcula qual será a pŕoxima página em relação a página atual em exibição */
        $proxima_pagina = ($pagina_atual < $ultima_pagina) ? $pagina_atual + 1 : 0;
        /* Cálcula qual será a página inicial do nosso range */
        $range_inicial = (($pagina_atual - RANGE_PAGINAS) >= 1) ? $pagina_atual - RANGE_PAGINAS : 1;
        /* Cálcula qual será a página final do nosso range */
        $range_final = (($pagina_atual + RANGE_PAGINAS) <= $ultima_pagina) ? $pagina_atual + RANGE_PAGINAS : $ultima_pagina;
        /* Verifica se vai exibir o botão "Primeiro" e "Pŕoximo" */
        $exibir_botao_inicio = ($range_inicial < $pagina_atual) ? '' : 'hidden';
        /* Verifica se vai exibir o botão "Anterior" e "Último" */
        $exibir_botao_final = ($range_final > $pagina_atual) ? '' : 'hidden';
        if (!empty($dados)):
            echo "
     <table class='table table-striped table-bordered'>
     <thead>
       <tr style='text-transform: uppercase;' class='active'>
        <th style='text-align: center; font-weight: bolder;'>ID</th>
        <th style='text-align: center; font-weight: bolder;'>Título</th>
        <th style='text-align: center; font-weight: bolder;'>ISBN</th>
        <th style='text-align: center; font-weight: bolder;'>Edição</th>
        <th style='text-align: center; font-weight: bolder;'>Ano</th>
        <th style='text-align: center; font-weight: bolder;'>Editora</th>
        <th style='text-align: center; font-weight: bolder;'>Categoria</th>";
        if($_SESSION['cargo'] > 1) { echo "<th style='text-align: center; font-weight: bolder;' colspan='2'>Ações</th>"; }
       echo "</tr>
     </thead>
     <tbody>";
            foreach ($dados as $source):
                echo "<tr>
        <td style='text-align: center'>$source->idtb_livro</td>
        <td style='text-align: center'>$source->titulo</td>
        <td style='text-align: center'>$source->isbn</td>
        <td style='text-align: center'>$source->edicao</td>
        <td style='text-align: center'>$source->ano</td>
        <td style='text-align: center'>$source->nomeEditora</td>
        <td style='text-align: center'>$source->nomeCategoria</td>";
        if($_SESSION['cargo'] > 1) { echo "<td style='text-align: center'><a href='?act=upd&id=$source->idtb_livro' title='Alterar'><i class='ti-reload'></i></a></td>
                                            <td style='text-align: center'><a href='?act=del&id=$source->idtb_livro' title='Remover'><i class='ti-close'></i></a></td>"; }
       echo "</tr>";
            endforeach;
            echo "
</tbody>
    </table>
     <div class='box-paginacao' style='text-align: center'>
       <a class='box-navegacao  $exibir_botao_inicio' href='$endereco?page=$primeira_pagina' title='Primeira Página'> Primeira  |</a>
       <a class='box-navegacao  $exibir_botao_inicio' href='$endereco?page=$pagina_anterior' title='Página Anterior'> Anterior  |</a>
";
            /* Loop para montar a páginação central com os números */
            for ($i = $range_inicial; $i <= $range_final; $i++):
                $destaque = ($i == $pagina_atual) ? 'destaque' : '';
                echo "<a class='box-numero $destaque' href='$endereco?page=$i'> ( $i ) </a>";
            endfor;
            echo "<a class='box-navegacao $exibir_botao_final' href='$endereco?page=$proxima_pagina' title='Próxima Página'>| Próxima  </a>
                  <a class='box-navegacao $exibir_botao_final' href='$endereco?page=$ultima_pagina'  title='Última Página'>| Última  </a>
     </div>";
        else:
            echo "<p class='bg-danger'>Nenhum registro foi encontrado!</p>
     ";
        endif;
    }

}