<?php

require_once "view/template.php";
require_once "dao/daoLivro.php";
require_once "dao/daoEditora.php";
require_once "dao/daoCategoria.php";
require_once "dao/daoAutor.php";
require_once "modelo/livro.php";
require_once "db/Conexao.php";

$object = new daoLivro();
$editoraDao = new daoEditora();
$categoriaDao = new daoCategoria();
$autorDao = new daoAutor();

$editoras = $editoraDao->getComboBoxEditora();
$categorias = $categoriaDao->getComboBoxCategoria();
$autores = $autorDao->getMultiSelectAutor();

template::header();
template::sidebar();
template::mainpanel();

$permissao = $_SESSION['cargo'] == 10 || $_SESSION['cargo'] == 3;

// Verificar se foi enviando dados via POST
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $id = (isset($_POST["id"]) && $_POST["id"] != null) ? $_POST["id"] : "";
    $titulo = (isset($_POST["titulo"]) && $_POST["titulo"] != null) ? $_POST["titulo"] : "";
    $isbn = (isset($_POST["isbn"]) && $_POST["isbn"] != null) ? $_POST["isbn"] : "";
    $edicao = (isset($_POST["edicao"]) && $_POST["edicao"] != null) ? $_POST["edicao"] : "";
    $ano = (isset($_POST["ano"]) && $_POST["ano"] != null) ? $_POST["ano"] : "";
    $upload = (isset($_POST["upload"]) && $_POST["upload"] != null) ? $_POST["upload"] : "";
    $editora = (isset($_POST["editora"]) && $_POST["editora"] != null) ? $_POST["editora"] : "";
    $categoria = (isset($_POST["categoria"]) && $_POST["categoria"] != null) ? $_POST["categoria"] : "";
} 
else if (!isset($id)) {
    // Se não se não foi setado nenhum valor para variável $id
    $id = (isset($_GET["id"]) && $_GET["id"] != null) ? $_GET["id"] : "";
    $titulo = null;
    $isbn = null;
    $edicao = null;
    $ano = null;
    $upload = null;
    $categoria = null;
}

if (isset($_REQUEST["act"]) && $_REQUEST["act"] == "upd" && $id != "") {
    $livro = new livro($id, null, null, null, null, null, null, null);
    $resultado = $object->atualizar($livro);
    $id = $resultado->getIdLivro();
    $titulo = $resultado->getTitulo();
    $isbn = $resultado->getIsbn();
    $edicao = $resultado->getEdicao();
    $ano = $resultado->getAno();
    $upload = $resultado->getUpload();
    $editora = $resultado->getEditora();
    $categoria = $resultado->getCategoria();
}

if (isset($_REQUEST["act"]) && $_REQUEST["act"] == "save") {
    $livro = new livro($id, $titulo, $isbn, $edicao, $ano, $upload, $editora, $categoria);
    $msg = $object->salvar($livro);
    $id = null;
    $titulo = null;
    $isbn = null;
    $edicao = null;
    $ano = null;
    $upload = null;
    $editora = null;
    $categoria = null;

}
if (isset($_REQUEST["act"]) && $_REQUEST["act"] == "del" && $id != "") {
    $livro = new livro($id, null, null, null, null, null, null, null);
    $msg = $object->remover($livro);
    $id = null;
}
?>

    <div class='content' xmlns="http://www.w3.org/1999/html">
        <div class='container-fluid'>
            <div class='row'>
                <div class='col-md-12'>
                    <div class='card'>
                        <div class='header'>
                            <h4 class='title'>Livros</h4>
                            <p class='category'>Lista de Livros do Sistema</p>

                        </div>
                        <?php if($permissao){?>
                        <div class='content table-responsive'>
                            <form action="?act=save&id=" method="POST" name="form1">

                                <input type="hidden" name="id" value="<?=$id?>"/>
                                <Label>Título</Label>
                                <input class="form-control" type="text" size="200" name="titulo" value="<?=$titulo?>" required/>
                                <br/>
                                <Label>ISBN</Label>
                                <input class="form-control" type="text" size="200" name="isbn" value="<?=$isbn?>" required/>
                                <br/>
                                <Label>Edição</Label>
                                <input class="form-control" type="text" size="4" name="edicao" value="<?=$edicao?>" style="width:10%; display: inline"/>
                                <label>Ano</label>
                                <input class="form-control" type="number" size="4" name="ano" value="<?=$ano?>" required style="width:10%; display: inline"/>
                                <!-- <label>Upload</label>
                                <input class="form-control" type="text" size="200" name="upload" value="<?=$upload?>" style="width:60%; display: inline"/> -->
                                <br /> <br />
                                <label>Editora</label>
                                <select name="editora" class="form-control">
                                    <?php
                                        foreach($editoras as $editora){
                                    ?>
                                    <option value="<?=$editora->idtb_editora?>"><?=$editora->nomeEditora?></option>
                                    <?php
                                    }
                                    ?>
                                </select>
                                <br /> <br />
                                <label>Categoria</label>
                                <select name="categoria" class="form-control">
                                    <?php
                                        foreach($categorias as $categoria){
                                    ?>
                                    <option value="<?=$categoria->idtb_categoria?>"><?=$categoria->nomeCategoria?></option>
                                    <?php
                                    }
                                    ?>
                                </select>
                                </select>
                                <br /> <br />
                                <label>Autores</label>
                                <select name="autores" multiple class="form-control">
                                    <?php
                                        foreach($autores as $autor){
                                    ?>
                                    <option value="<?=$autor->idtb_autores?>"><?=$autor->nomeAutor?></option>
                                    <?php
                                    }
                                    ?>
                                </select>
                                <br /> <br />
                                <input class="btn btn-success" type="submit" value="REGISTRAR">
                                <hr>
                            </form>
                            <?php
                            }
                                echo (isset($msg) && ($msg != null || $msg != "")) ? $msg : '';
                                $object->tabelapaginada();
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php
template::footer();
?>