<?php

require_once "view/template.php";

template::header();
template::sidebar();
template::mainpanel();

?>

<div class='content' xmlns="http://www.w3.org/1999/html">
    <div class='container-fluid'>
        <div class='row'>
            <div class='col-md-12'>
                <div class='card'>
                    <div class='header'>
                        <h4 class='title'>Relatórios</h4>
                        <p class='category'>Relatórios</p>

                    </div>
                    <div class='content table-responsive'>
                        <form action="gerarRelatorio.php" method="POST" name="form1">
                            <Label>De</Label>
                            <input type="date" class="form-control" name="dataDe" required />
                            <Label>Até</Label>
                            <input class="form-control" type="date" name="dataAte" required />
                            <br/>
                            <input class="btn btn-success" type="submit" value="GERAR">
                            <hr>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php 
template::footer();
?>