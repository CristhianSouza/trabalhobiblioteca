<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <link rel='icon' type='image/png' sizes='96x96' href='assets/img/favicon.jpg'>
        <meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1' />

        <title>Biblioteca</title>

        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
        <meta name='viewport' content='width=device-width' />

        <!-- Bootstrap core CSS     -->
        <link href="assets/css/bootstrap.min.css" rel='stylesheet' />

        <!-- Animation library for notifications   -->
        <link href='assets/css/animate.min.css' rel='stylesheet'/>

        <!--  Paper Dashboard core CSS    -->
        <link href='assets/css/paper-dashboard.css' rel='stylesheet'/>

        <!--  Fonts and icons     -->
        <link href='http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css' rel='stylesheet'>
        <link href='https://fonts.googleapis.com/css?family=Muli:400,300' rel='stylesheet' type='text/css'>
        <link href='assets/css/themify-icons.css' rel='stylesheet'>
        <title>Logar no Sistema</title>
    </head>
    <body style="background-color:lightblue">
        <div class="form-horizontal" >
            <div class="col-md-offset-4 col-md-4">
                <?php
                    if (isset($_REQUEST["msg"]) && $_REQUEST["msg"] == 1) {
                        echo "<div style='padding: 15px; background-color: lightblue' class='text-center'>
                                  Um email foi enviado para alterar sua senha!
                              </div>";
                    }
                    else if(isset($_REQUEST["msg"]) && $_REQUEST["msg"] == 2){
                        echo "<div style='padding: 15px; background-color: lightblue' class='text-center'>
                                  Senha alterada com sucesso!
                              </div>";
                    }
                ?>
                <form method="post" action="logar.php" id="formlogin" name="formlogin" style="background-color:white; padding: 50px; margin-top: 50px">
                    <fieldset id="fie" class="text-center">
                        <a href='index.php'><img src="assets/img/logo.jpg" height="150" width="200"></a>
                        <legend>LOGIN</legend><br />
                        <label>NOME : </label>
                        <input type="text" name="login" id="login" class="form-control" /><br /><br />
                        <label>SENHA :</label>
                        <input type="password" name="senha" id="senha" class="form-control" /><br /><br />
                        <a href="esqueciMinhaSenha.php">Esqueci minha senha</a><br /><br />
                        <input type="submit" value="LOGAR" />
                    </fieldset>
                </form>
            </div>
        </div>
    </body>
</html>