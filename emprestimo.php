<?php

    require_once "dao/daoEmprestimo.php";
    require_once "dao/daoExemplar.php";
    require_once "dao/daoUsuario.php";
    require_once "modelo/emprestimo.php";
    require_once "db/Conexao.php";
    require_once "view/template.php";

    $object = new daoEmprestimo();

    $daoExemplar = new daoExemplar();
    $exemplares = $daoExemplar->getComboBoxExemplar();
    
    $daoUsuario = new daoUsuario();
    $usuarios = $daoUsuario->getComboBoxUsuario();

    template::header();
    template::sidebar();
    template::mainpanel();

    // Verificar se foi enviando dados via POST
    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        $idExemplar = (isset($_POST["idExemplar"]) && $_POST["idExemplar"] != null) ? $_POST["idExemplar"] : "";
        $idUsuario = (isset($_POST["idUsuario"]) && $_POST["idUsuario"] != null) ? $_POST["idUsuario"] : "";
        $dataEmprestimo = (isset($_POST["dataEmprestimo"]) && $_POST["dataEmprestimo"] != null) ? $_POST["dataEmprestimo"] : "";
        $observacao = (isset($_POST["observacao"]) && $_POST["observacao"] != null) ? $_POST["observacao"] : "";
    } 
    else if (!isset($idExemplar) && !isset($idUsuario)) {
        // Se não se não foi setado nenhum valor para variável $id
        $idExemplar = (isset($_POST["idExemplar"]) && $_POST["idExemplar"] != null) ? $_POST["idExemplar"] : "";
        $idUsuario = (isset($_POST["idUsuario"]) && $_POST["idUsuario"] != null) ? $_POST["idUsuario"] : "";
        $dataEmprestimo = null;
        $observacao = null;
        $dataDevolucao = null;
    }

    if (isset($_REQUEST["act"]) && $_REQUEST["act"] == "upd" && $idExemplar != "" && $idUsuario != "") {
        $emprestimo = new emprestimo($idUsuario, $idExemplar, "", "", "");
        $resultado = $object->atualizar($emprestimo);
        $idExemplar = $resultado->getExemplar();
        $dataEmprestimo = $resultado->getDataEmprestimo();
        $observacao = $resultado->getObservacao();
        $idUsuario = $resultado->getUsuario();
    }

    if (isset($_REQUEST["act"]) && $_REQUEST["act"] == "save" && $idExemplar != "" && $idExemplar != "" && $observacao != "" ) {
        if(!$object->verificarDebito($idUsuario)){
            if(!$object->verificarQuantidadeEmprestadaMaxima($idUsuario)){
                $emprestimo = new emprestimo(null, $idUsuario, $idExemplar, $dataEmprestimo, $observacao);
                $msg = $object->salvar($emprestimo);
                $idExemplar = null;
                $dataEmprestimo = null;
                $observacao = null;
                $idUsuario = null;
            }
            else
                echo "<script> alert('Não foi possível, pois o usuário está com o muitos livros não devolvidos!'); </script>";
        }
        else
            echo "<script> alert('Não foi possível realizar empréstimo, pois o usuário está em débito!'); </script>";
    }

    if (isset($_REQUEST["act"]) && $_REQUEST["act"] == "del") {
        $idEmprestimo = $_GET["idEmprestimo"];
        $msg = $object->remover($idEmprestimo);
        $idExemplar = null;
        $dataEmprestimo = null;
        $observacao = null;
        $idUsuario = null;
    }
?>

    <div class='content' xmlns="http://www.w3.org/1999/html">
        <div class='container-fluid'>
            <div class='row'>
                <div class='col-md-12'>
                    <div class='card'>
                        <div class='header'>
                            <h4 class='title'>Emprestimo</h4>
                            <p class='category'>Lista de Emprestimos do Sistema</p>

                        </div>
                        <div class='content table-responsive'>
                            <form action="?act=save&id=" method="POST" name="form1">
                                
                                <label>Exemplar</label>
                                <select name="idExemplar" class="form-control">
                                    <?php
                                        foreach($exemplares as $exem){
                                            ?>
                                    <option value="<?=$exem->idtb_exemplar?>"><?=$exem->titulo?></option>
                                    <?php
                                    }
                                    ?>
                                </select>
                                <br /> <br />
                                <label>Usuário</label>
                                <select name="idUsuario" class="form-control">
                                    <?php
                                        foreach($usuarios as $usua){
                                            ?>
                                    <option value="<?=$usua->idtb_usuario?>"><?=$usua->nomeUsuario?></option>
                                    <?php
                                    }
                                    ?>
                                </select>
                                <br /> <br />
                                <label>Data de Empréstimo</label>
                                <input class="form-control" type="date" name="dataEmprestimo" size="50" value="<?php
                                // Preenche o id no campo id com um valor "value"
                                echo (isset($dataEmprestimo) && ($dataEmprestimo != null || $dataEmprestimo != "")) ? $dataEmprestimo : '';
                                ?>"/>
                                <br /> <br />
                                <Label>Observação</Label>
                                <input class="form-control" type="text" size="50" name="observacao" value="<?php
                                // Preenche o nome no campo nome com um valor "value"
                                echo (isset($observacao) && ($observacao != null || $observacao != "")) ? $observacao : '';
                                ?>" />
                                <br/>
                                <input class="btn btn-success" type="submit" value="REGISTRAR">
                                <hr>
                            </form>
                            <?php
                                echo (isset($msg) && ($msg != null || $msg != "")) ? $msg : '';
                                //chamada a paginação
                                $object->tabelapaginada();
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php
template::footer();
?>