<?php
    
require_once "./vendor/davefx/phplot/phplot/phplot.php";
require_once "dao/daoEmprestimo.php";
require_once "db/Conexao.php";

$daoEmp = new daoEmprestimo();

$dados = $daoEmp->obterEmprestadosPorCategoria();

$data = array();

foreach($dados as $dado){
    array_push($data, array($dado->nomeCategoria, $dado->total));
}

    $plot = new PHPlot(480 , 350);

    $plot->SetTitle(utf8_decode("Livros emprestados nos últimos 3 meses por categoria\n"));
    
    $plot->SetPrecisionY(1);
    
    $plot->SetPlotType("bars");
    
    $plot->SetDataType("text-data");
    
    $plot->SetDataValues($data);
    
    $plot->SetXTickPos('none');
    
    $plot->SetXLabel("Categorias");
    
    $plot->SetXLabelFontSize(2);
    $plot->SetAxisFontSize(2);
    
    $plot->SetYDataLabelPos('plotin');
    
    $plot->DrawGraph();
        