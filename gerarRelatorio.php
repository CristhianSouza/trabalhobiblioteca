<?php
$dataDe = $_POST["dataDe"];
$dataAte = $_POST["dataAte"];


require_once "dao/daoLivro.php";
require_once "modelo/livro.php";
require_once "db/Conexao.php";
require_once "view/template.php";

template::header();
template::sidebar();
template::mainpanel();

$daoLivro = new daoLivro();
$relatorio = $daoLivro->gerarRelatorioLivros($dataDe, $dataAte);
?>

<div class='content' xmlns="http://www.w3.org/1999/html">
    <div class='container-fluid'>
        <div class='row'>
            <div class='col-md-12'>
                <div class='card'>
                    <div class='header'>
                        <h4 class='title'>Relatório</h4>
                        <p class='category'>Relatório de <?=$dataDe?> até <?=$dataAte?></p>

                    </div>
                    <div class='content table-responsive'>
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Livro</th>
                                    <th>Empréstimos realizados</th>
                                    <th>Exemplares possuídos</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <?php foreach($relatorio as $item){?>
                                        <tr>
                                            <td><?=$item->Titulo?></td>
                                            <td><?=$item->Emprestimos?></td>
                                            <td><?=$item->Exemplares?></td>
                                        </tr>
                                    <?php
                                    }
                                    ?>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
template::footer()
?>