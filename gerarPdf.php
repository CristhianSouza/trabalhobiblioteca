<?php
require_once "./vendor/fpdf/fpdf/original/mem_image.php";
require_once "livrosEmprestadosCategoriaGraphPdf.php";
require_once "livrosEmprestadosGraphPdf.php";
require_once "livrosReservadosCategoriaGraphPdf.php";
require_once "livrosReservadosTresMesesPdf.php";
require_once "livrosReservasGraphPdf.php";


$pdf = new PDF_MemImage();
$pdf->AddPage();
$pdf->GDImage($livrosEmprestadosCategoria->img, 30, 30, 150);
$pdf->GDImage($livrosEmprestadosPdf->img, 30, 160, 150);

$pdf->AddPage();
$pdf->GDImage($livrosReservadosCategoriaPdf->img, 30, 30, 150);
$pdf->GDImage($livrosEmprestadosTresMeses->img, 30, 160, 150);

$pdf->AddPage();
$pdf->GDImage($livrosReservadosPdf->img, 30, 30, 150);

$pdf->Output();
?>