<?php
    
require_once "./vendor/davefx/phplot/phplot/phplot.php";
require_once "dao/daoEmprestimo.php";
require_once "db/Conexao.php";

$daoEmp = new daoEmprestimo();

$ultimoMes = $daoEmp->obterEmprestadosUltimoMes();
$penultimoMes = $daoEmp->obterEmprestadosPenultimoMes();
$anteMes = $daoEmp->obterEmprestadosAntePenultimoMes();

$data = array(array($anteMes->mes, $anteMes->total), 
      array($penultimoMes->mes, $penultimoMes->total),
      array($ultimoMes->mes, $ultimoMes->total)
);

$plot = new PHPlot(480 , 350);

$plot->SetTitle(utf8_decode("Livros emprestados nos últimos 3 meses\n"));

$plot->SetPrecisionY(1);

$plot->SetPlotType("bars");

$plot->SetDataType("text-data");

$plot->SetDataValues($data);

$plot->SetXTickPos('none');

$plot->SetXLabel("Meses");

$plot->SetXLabelFontSize(2);
$plot->SetAxisFontSize(2);

$plot->SetYDataLabelPos('plotin');

$plot->DrawGraph();