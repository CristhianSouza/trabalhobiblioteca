<?php
    
require_once "./vendor/davefx/phplot/phplot/phplot.php";
require_once "dao/daoEmprestimo.php";
require_once "db/Conexao.php";

$daoEmp = new daoEmprestimo();

$ultimoMes = $daoEmp->obterEmprestadosUltimoMes();
$penultimoMes = $daoEmp->obterEmprestadosPenultimoMes();
$anteMes = $daoEmp->obterEmprestadosAntePenultimoMes();

$data = array(array($anteMes->mes, $anteMes->total), 
      array($penultimoMes->mes, $penultimoMes->total),
      array($ultimoMes->mes, $ultimoMes->total)
);

$livrosEmprestadosPdf = new PHPlot(480 , 350);

$livrosEmprestadosPdf->SetTitle(utf8_decode("Livros emprestados nos últimos 3 meses\n"));

$livrosEmprestadosPdf->SetPrecisionY(1);

$livrosEmprestadosPdf->SetPlotType("bars");

$livrosEmprestadosPdf->SetDataType("text-data");

$livrosEmprestadosPdf->SetDataValues($data);

$livrosEmprestadosPdf->SetXTickPos('none');

$livrosEmprestadosPdf->SetXLabel("Meses");

$livrosEmprestadosPdf->SetXLabelFontSize(2);
$livrosEmprestadosPdf->SetAxisFontSize(2);

$livrosEmprestadosPdf->SetYDataLabelPos('plotin');

$livrosEmprestadosPdf->SetPrintImage(false);

$livrosEmprestadosPdf->DrawGraph();