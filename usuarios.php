<?php

require_once "view/template.php";
require_once "dao/daoUsuario.php";
require_once "modelo/usuario.php";
require_once "db/Conexao.php";

$object = new daoUsuario();

template::header();
template::sidebar();
template::mainpanel();

// Verificar se foi enviando dados via POST
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $id = (isset($_POST["id"]) && $_POST["id"] != null) ? $_POST["id"] : "";
    $nome = (isset($_POST["nome"]) && $_POST["nome"] != null) ? $_POST["nome"] : "";
    $tipo = (isset($_POST["tipo"]) && $_POST["tipo"] != null) ? $_POST["tipo"] : "";
    $senha = (isset($_POST["senha"]) && $_POST["senha"] != null) ? $_POST["senha"] : "";
    $email = (isset($_POST["email"]) && $_POST["email"] != null) ? $_POST["email"] : "";
} else if (!isset($id)) {
    // Se não se não foi setado nenhum valor para variável $id
    $id = (isset($_GET["id"]) && $_GET["id"] != null) ? $_GET["id"] : "";
    $nome = null;
    $tipo = null;
    $senha = null;
    $email = null;
}

if (isset($_REQUEST["act"]) && $_REQUEST["act"] == "upd" && $id != "") {
    $usuario = new usuario($id, null, null, null, null);
    $resultado = $object->atualizar($usuario);
    $id = $resultado->getIdtbUsuario();
    $nome = $resultado->getNomeUsuario();
    $tipo = $resultado->getTipo();
    $senha = $resultado->getSenha();
    $email = $resultado->getEmail();

}

if (isset($_REQUEST["act"]) && $_REQUEST["act"] == "save" && $nome != "" ) {
    $usuario = new usuario($id, $nome, $tipo, $senha, $email);
    $msg = $object->salvar($usuario);
    $id = null;
    $nome = null;
    $tipo = null;
    $senha = null;
    $email = null;

}
if (isset($_REQUEST["act"]) && $_REQUEST["act"] == "del" && $id != "") {
    $usuario = new usuario($id, null, null, null, null);
    $msg = $object->remover($usuario);
    $id = null;
}
?>

    <div class='content' xmlns="http://www.w3.org/1999/html">
        <div class='container-fluid'>
            <div class='row'>
                <div class='col-md-12'>
                    <div class='card'>
                        <div class='header'>
                            <h4 class='title'>Autores</h4>
                            <p class='category'>Lista de Autores do Sistema</p>

                        </div>
                        <div class='content table-responsive'>
                            <form action="?act=save&id=" method="POST" name="form1">

                                <input type="hidden" name="id" value="<?php
                                // Preenche o id no campo id com um valor "value"
                                echo (isset($id) && ($id != null || $id != "")) ? $id : '';
                                ?>"/>
                                <label>Cargo</label>
                                <select name="tipo" class="form-control">
                                    <option value="10" <?php if($tipo == 10) {echo 'selected';} ?>>Admin</option>
                                    <option value="8" <?php if($tipo == 8) {echo 'selected';} ?>>Professor</option>
                                    <option value="1" <?php if($tipo == 1) {echo 'selected';} ?>>Aluno</option>
                                    <option value="3" <?php if($tipo == 3) {echo 'selected';} ?>>Bibliotecario</option>
                                </select>
                                <br /> <br />

                                <Label>Nome</Label>
                                <input class="form-control" type="text" size="50" name="nome" value="<?php
                                // Preenche o nome no campo nome com um valor "value"
                                echo (isset($nome) && ($nome != null || $nome != "")) ? $nome : '';
                                ?>" required/>
                                <br/>

                                <Label>Senha</Label>
                                <input class="form-control" type="text" size="50" name="senha" value="<?php
                                // Preenche o nome no campo nome com um valor "value"
                                echo (isset($senha) && ($senha != null || $senha != "")) ? $senha : '';
                                ?>" required/>
                                <br/>

                                <Label>Email</Label>
                                <input class="form-control" type="email" size="50" name="email" value="<?php
                                // Preenche o nome no campo nome com um valor "value"
                                echo (isset($email) && ($email != null || $email != "")) ? $email : '';
                                ?>" required/>
                                <br/>

                                <input class="btn btn-success" type="submit" value="REGISTRAR">
                                <hr>
                            </form>
                            <?php
                                echo (isset($msg) && ($msg != null || $msg != "")) ? $msg : '';
                                //chamada a paginação
                                $object->tabelapaginada();
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php
template::footer();
?>