<?php
/**
 * Created by PhpStorm.
 * User: tassio
 * Date: 04/01/2018
 * Time: 16:31
 */

require_once 'dao/daoUsuario.php';
require_once "db/Conexao.php";

// session_start inicia a sessão
session_start();
// as variáveis login e senha recebem os dados digitados na página anterior
$login = $_POST['login'];
$senha = $_POST['senha'];

$usuario = new daoUsuario();

$valido = $usuario->logar($login, $senha);

if($valido != null)
{
    $_SESSION['idUsuario'] = $valido->idtb_usuario;
    $_SESSION['login'] = $login;
    $_SESSION['cargo'] = $valido->tipo;
    $_SESSION['senha'] = $senha;
}
else{
    unset ($_SESSION['login']);
    unset ($_SESSION['senha']);
}
header('location:index.php');

?>