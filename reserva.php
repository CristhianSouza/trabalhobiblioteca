<?php

require_once "view/template.php";
require_once "dao/daoReserva.php";
require_once "dao/daoExemplar.php";
require_once "modelo/reserva.php";
require_once "db/Conexao.php";

$object = new daoReserva();
$daoExemplar = new daoExemplar();
$exemplares = $daoExemplar->getComboBoxExemplar();

template::header();
template::sidebar();
template::mainpanel();

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $id = (isset($_POST["id"]) && $_POST["id"] != null) ? $_POST["id"] : "";
    $exemplar = (isset($_POST["exemplar"]) && $_POST["exemplar"] != null) ? $_POST["exemplar"] : "";
    $usuario = $_SESSION['idUsuario'];
    $usuarioCancela = (isset($_POST["usuarioCancela"]) && $_POST["usuarioCancela"] != null) ? $_POST["usuarioCancela"] : "";
    $dataReserva = date('Y-m-d H:i:s');
} 
else if (!isset($id)) {
    $id = (isset($_GET["id"]) && $_GET["id"] != null) ? $_GET["id"] : "";
    $exemplar = null;
    $usuario = null;
    $usuarioCancela = null;
    $dataReserva = null;
}

if (isset($_REQUEST["act"]) && $_REQUEST["act"] == "save") {
    $reserva = new reserva($id, $exemplar, $usuario, $usuarioCancela, $dataReserva);
    $msg = $object->salvar($reserva);
    $id = null;
    $exemplar = null;
    $usuario = null;
    $usuarioCancela = null;
    $dataReserva = null;

}

if (isset($_REQUEST["act"]) && $_REQUEST["act"] == "del" && $id != "") {
    $msg = $object->remover($id);
    $id = null;
}
?>

    <div class='content' xmlns="http://www.w3.org/1999/html">
        <div class='container-fluid'>
            <div class='row'>
                <div class='col-md-12'>
                    <div class='card'>
                        <div class='header'>
                            <h4 class='title'>Reserva</h4>
                            <p class='category'>Lista de Reservas</p>

                        </div>
                        <div class='content table-responsive'>
                            <form action="?act=save&id=" method="POST" name="form1">

                                <input type="hidden" name="id" value="<?php
                                // Preenche o id no campo id com um valor "value"
                                echo (isset($id) && ($id != null || $id != "")) ? $id : '';
                                ?>"/>
                                <label>Livro</label>
                                <select name="exemplar">
                                    <?php foreach($exemplares as $exemplar){?>
                                    <option value="<?=$exemplar->idtb_exemplar?>"><?=$exemplar->titulo?></option>
                                    <?php
                                    }
                                    ?>
                                </select>
                                <br /> <br />
                                <input class="btn btn-success" type="submit" value="RESERVAR">
                                <hr>
                            </form>
                            <?php
                                echo (isset($msg) && ($msg != null || $msg != "")) ? $msg : '';
                                //chamada a paginação
                                $object->tabelapaginada();
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php
template::footer();
?>0