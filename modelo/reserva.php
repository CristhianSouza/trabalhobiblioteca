<?php
/**
 * Created by PhpStorm.
 * User: tassio
 * Date: 2019-03-11
 * Time: 21:39
 */

class reserva
{

    private $id_reserva;
    private $id_exemplar;
    private $id_usuario;
    private $id_usuario_cancela;
    private $data_reserva;

    public function __construct($id_reserva, $id_exemplar, $id_usuario, $id_usuario_cancela, $data_reserva)
    {
        $this->id_reserva = $id_reserva;
        $this->id_exemplar = $id_exemplar;
        $this->id_usuario = $id_usuario;
        $this->id_usuario_cancela = $id_usuario_cancela;
        $this->data_reserva = $data_reserva;
    }

    public function getIdReserva()
    {
        return $this->id_reserva;
    }

    public function setIdReserva($id_reserva)
    {
        $this->id_reserva = $id_reserva;
    }

    public function getIdExemplar()
    {
        return $this->id_exemplar;
    }

    public function setIdExemplar($id_exemplar)
    {
        $this->id_exemplar = $id_exemplar;
    }

    public function getIdUsuario()
    {
        return $this->id_usuario;
    }

    public function setIdUsuario($id_usuario)
    {
        $this->id_usuario = $id_usuario;
    }

    public function getIdUsuarioCancela()
    {
        return $this->id_usuario_cancela;
    }

    public function setIdUsuarioCancela($id_usuario_cancela)
    {
        $this->id_usuario_cancela = $id_usuario_cancela;
    }

    public function getDataReserva()
    {
        return $this->data_reserva;
    }

    public function setDataReserva($data_reserva)
    {
        $this->data_reserva = $data_reserva;
    }

}