<?php
    require_once 'dao/daoUsuario.php';
    require_once "db/Conexao.php";
    $id = $_GET['token'];

    $usuario = new daoUsuario();
    $alterar = $usuario->verificarAlterarSenha($id);

    if(!$alterar){
        header('Location: login.php');
    }
    else{
        echo '<!DOCTYPE html>
        <html lang="en">
            <head>
                <meta charset="UTF-8">
                <link rel="icon" type="image/png" sizes="96x96" href="assets/img/favicon.jpg">
                <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        
                <title>Biblioteca</title>
        
                <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport" />
                <meta name="viewport" content="width=device-width" />
        
                <!-- Bootstrap core CSS     -->
                <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
        
                <!-- Animation library for notifications   -->
                <link href="assets/css/animate.min.css" rel="stylesheet"/>
        
                <!--  Paper Dashboard core CSS    -->
                <link href="assets/css/paper-dashboard.css" rel="stylesheet"/>
        
                <!--  Fonts and icons     -->
                <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
                <link href="https://fonts.googleapis.com/css?family=Muli:400,300" rel="stylesheet" type="text/css">
                <link href="assets/css/themify-icons.css" rel="stylesheet">
                <title>Logar no Sistema</title>
            </head>
            <body>
                <div class="form-horizontal">
                    <div class="col-md-offset-4 col-md-4">
                        <form method="post" action="novaSenha.php" id="formRecuperar" name="formRecuperar" >
                            <fieldset id="fie">
                                <legend>ALTERAR SENHA</legend><br />
                                <label>NOVA SENHA : </label>
                                <input type="password" name="senha" id="senha" /><br /><br />
                                <input type="hidden" name="id" id="id" value="'.$id.'" />
                                <input type="submit" value="ALTERAR" />
                            </fieldset>
                        </form>
                    </div>
                </div>
            </body>
        </html>';
    }
?>