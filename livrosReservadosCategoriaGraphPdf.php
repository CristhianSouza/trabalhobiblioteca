<?php
    
require_once "./vendor/davefx/phplot/phplot/phplot.php";
require_once "dao/daoReserva.php";
require_once "db/Conexao.php";

$daoEmp = new daoReserva();

$dados = $daoEmp->obterReservadosPorCategoria();

$data = array();

foreach($dados as $dado){
    array_push($data, array($dado->nomeCategoria, $dado->total));
}

    $livrosReservadosCategoriaPdf = new PHPlot(480 , 350);

    $livrosReservadosCategoriaPdf->SetTitle(utf8_decode("Livros reservados nos últimos 3 meses por categoria\n"));
    
    $livrosReservadosCategoriaPdf->SetPrecisionY(1);
    
    $livrosReservadosCategoriaPdf->SetPlotType("bars");
    
    $livrosReservadosCategoriaPdf->SetDataType("text-data");
    
    $livrosReservadosCategoriaPdf->SetDataValues($data);
    
    $livrosReservadosCategoriaPdf->SetXTickPos('none');
    
    $livrosReservadosCategoriaPdf->SetXLabel("Categorias");
    
    $livrosReservadosCategoriaPdf->SetXLabelFontSize(2);
    $livrosReservadosCategoriaPdf->SetAxisFontSize(2);
    
    $livrosReservadosCategoriaPdf->SetYDataLabelPos('plotin');
    
    $livrosReservadosCategoriaPdf->SetPrintImage(false);

    $livrosReservadosCategoriaPdf->DrawGraph();
        