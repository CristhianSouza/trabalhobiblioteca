<?php
    
require_once "./vendor/davefx/phplot/phplot/phplot.php";
require_once "dao/daoReserva.php";
require_once "db/Conexao.php";

$daoEmp = new daoReserva();

$ultimoMes = $daoEmp->obterReservadosUltimoMes();
$penultimoMes = $daoEmp->obterReservadosPenultimoMes();
$anteMes = $daoEmp->obterReservadosAntePenultimoMes();

$data = array(array($anteMes->mes, $anteMes->total), 
      array($penultimoMes->mes, $penultimoMes->total),
      array($ultimoMes->mes, $ultimoMes->total)
);

$livrosEmprestadosTresMeses = new PHPlot(480 , 350);

$livrosEmprestadosTresMeses->SetTitle(utf8_decode("Livros reservados nos últimos 3 meses\n"));

$livrosEmprestadosTresMeses->SetPrecisionY(1);

$livrosEmprestadosTresMeses->SetPlotType("bars");

$livrosEmprestadosTresMeses->SetDataType("text-data");

$livrosEmprestadosTresMeses->SetDataValues($data);

$livrosEmprestadosTresMeses->SetXTickPos('none');

$livrosEmprestadosTresMeses->SetXLabel("Meses");

$livrosEmprestadosTresMeses->SetXLabelFontSize(2);
$livrosEmprestadosTresMeses->SetAxisFontSize(2);

$livrosEmprestadosTresMeses->SetYDataLabelPos('plotin');

$livrosEmprestadosTresMeses->SetPrintImage(false);

$livrosEmprestadosTresMeses->DrawGraph();