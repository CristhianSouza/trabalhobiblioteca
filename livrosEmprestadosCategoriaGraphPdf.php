<?php
    
require_once "./vendor/davefx/phplot/phplot/phplot.php";
require_once "dao/daoEmprestimo.php";
require_once "db/Conexao.php";

$daoEmp = new daoEmprestimo();

$dados = $daoEmp->obterEmprestadosPorCategoria();

$data = array();

foreach($dados as $dado){
    array_push($data, array($dado->nomeCategoria, $dado->total));
}

    $livrosEmprestadosCategoria = new PHPlot(480 , 350);

    $livrosEmprestadosCategoria->SetTitle(utf8_decode("Livros emprestados nos últimos 3 meses por categoria\n"));
    
    $livrosEmprestadosCategoria->SetPrecisionY(1);
    
    $livrosEmprestadosCategoria->SetPlotType("bars");
    
    $livrosEmprestadosCategoria->SetDataType("text-data");
    
    $livrosEmprestadosCategoria->SetDataValues($data);
    
    $livrosEmprestadosCategoria->SetXTickPos('none');
    
    $livrosEmprestadosCategoria->SetXLabel("Categorias");
    
    $livrosEmprestadosCategoria->SetXLabelFontSize(2);
    $livrosEmprestadosCategoria->SetAxisFontSize(2);
    
    $livrosEmprestadosCategoria->SetYDataLabelPos('plotin');
    
    $livrosEmprestadosCategoria->SetPrintImage(false);
    $livrosEmprestadosCategoria->DrawGraph();

?>