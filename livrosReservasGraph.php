<?php
    
require_once "./vendor/davefx/phplot/phplot/phplot.php";
require_once "dao/daoEmprestimo.php";
require_once "dao/daoReserva.php";
require_once "db/Conexao.php";

$daoEmp = new daoEmprestimo();
$daoRes = new daoReserva();

$ultimoMesEmp = $daoEmp->obterEmprestadosUltimoMes();
$ultimoMesRes = $daoRes->obterReservadosUltimoMes();

$data = array(array("Emprestados", $ultimoMesEmp->total), 
      array("Reservados", $ultimoMesRes->total),
);

$plot = new PHPlot(480 , 350);

$plot->SetTitle(utf8_decode("Livros emprestados e reservados no último mês\n"));

$plot->SetPrecisionY(1);

$plot->SetPlotType("bars");

$plot->SetDataType("text-data");

$plot->SetDataValues($data);

$plot->SetXTickPos('none');

$plot->SetXLabel(" ");

$plot->SetXLabelFontSize(2);
$plot->SetAxisFontSize(2);

$plot->SetYDataLabelPos('plotin');

$plot->DrawGraph();