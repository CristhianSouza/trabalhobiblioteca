<?php

require_once "view/template.php";
require_once "dao/daoExemplar.php";
require_once "dao/daoLivro.php";
require_once "modelo/exemplar.php";
require_once "db/Conexao.php";

$object = new daoExemplar();
$livroDao = new daoLivro();

template::header();
template::sidebar();
template::mainpanel();

$livros = $livroDao->getComboBoxLivro();

// Verificar se foi enviando dados via POST
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $id = (isset($_POST["id"]) && $_POST["id"] != null) ? $_POST["id"] : "";
    $livro = (isset($_POST["livro"]) && $_POST["livro"] != null) ? $_POST["livro"] : "";
    $tipo = (isset($_POST["tipo"]) && $_POST["tipo"] != null) ? $_POST["tipo"] : "";
} else if (!isset($id)) {
    // Se não se não foi setado nenhum valor para variável $id
    $id = (isset($_GET["id"]) && $_GET["id"] != null) ? $_GET["id"] : "";
    $nome = null;
}

if (isset($_REQUEST["act"]) && $_REQUEST["act"] == "upd" && $id != "") {
    $exemplar = new exemplar($id, null, null);
    $resultado = $object->atualizar($exemplar);
    $id = $resultado->getIdExemplar();
    $livro = $resultado->getIdLivro();
    $tipo = $resultado->getTipoLivro();
}

if (isset($_REQUEST["act"]) && $_REQUEST["act"] == "save") {
    $exemplar = new exemplar($id, $livro, $tipo);
    $msg = $object->salvar($exemplar);
    $id = null;
    $livro = null;
    $tipo = null;

}
if (isset($_REQUEST["act"]) && $_REQUEST["act"] == "del" && $id != "") {
    $exemplar = new exemplar($id, null, null);
    $msg = $object->remover($exemplar);
    $id = null;
}
?>

    <div class='content' xmlns="http://www.w3.org/1999/html">
        <div class='container-fluid'>
            <div class='row'>
                <div class='col-md-12'>
                    <div class='card'>
                        <div class='header'>
                            <h4 class='title'>Exemplares</h4>
                            <p class='category'>Lista de Exemplares do Sistema</p>

                        </div>
                        <div class='content table-responsive'>
                            <form action="?act=save&id=" method="POST" name="form1">

                                <input type="hidden" name="id" value="<?php
                                // Preenche o id no campo id com um valor "value"
                                echo (isset($id) && ($id != null || $id != "")) ? $id : '';
                                ?>"/>
                                <Label>Circular</Label>
                                <select name="tipo" class="form-control">
                                    <option value="1">Sim</option>
                                    <option value="0">Não</option>
                                </select>
                                <br /> <br />
                                <label>Livro</label>
                                <select name="livro" class="form-control">
                                    <?php
                                        foreach($livros as $item){
                                    ?>
                                    <option value="<?=$item->idtb_livro?>"><?=$item->titulo?></option>
                                    <?php
                                    }
                                    ?>
                                </select>
                                <br /> <br />
                                <input class="btn btn-success" type="submit" value="REGISTRAR">
                                <hr>
                            </form>
                            <?php
                                echo (isset($msg) && ($msg != null || $msg != "")) ? $msg : '';
                                //chamada a paginação
                                $object->tabelapaginada();
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php
template::footer();
?>