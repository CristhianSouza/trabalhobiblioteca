<?php
    
require_once "./vendor/davefx/phplot/phplot/phplot.php";
require_once "dao/daoEmprestimo.php";
require_once "dao/daoReserva.php";
require_once "db/Conexao.php";

$daoEmp = new daoEmprestimo();
$daoRes = new daoReserva();

$ultimoMesEmp = $daoEmp->obterEmprestadosUltimoMes();
$ultimoMesRes = $daoRes->obterReservadosUltimoMes();

$data = array(array("Emprestados", $ultimoMesEmp->total), 
      array("Reservados", $ultimoMesRes->total),
);

$livrosReservadosPdf = new PHPlot(480 , 350);

$livrosReservadosPdf->SetTitle(utf8_decode("Livros emprestados e reservados no último mês\n"));

$livrosReservadosPdf->SetPrecisionY(1);

$livrosReservadosPdf->SetPlotType("bars");

$livrosReservadosPdf->SetDataType("text-data");

$livrosReservadosPdf->SetDataValues($data);

$livrosReservadosPdf->SetXTickPos('none');

$livrosReservadosPdf->SetXLabel(" ");

$livrosReservadosPdf->SetXLabelFontSize(2);
$livrosReservadosPdf->SetAxisFontSize(2);

$livrosReservadosPdf->SetYDataLabelPos('plotin');

$livrosReservadosPdf->SetPrintImage(false);

$livrosReservadosPdf->DrawGraph();